<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->ci =& get_instance();
		if ($this->session->userdata('login') != 'login'){
			redirect('login');
		}
		date_default_timezone_set('Asia/Makassar');
		setlocale(LC_ALL, 'IND');
	}

	public function index()
	{
		$this->beranda();
	}

	public function beranda(){
		$config_pagination = [
			'base_url' => '/blog/beranda',
			'total_rows' => count($this->db->get('blog')->result()),
			'per_page' => 10,
			'first_link' => 'Pertama',
			'last_link' => 'Terakhir',
			'next_link' => 'Berikutnya',
			'prev_link' => 'Sebelumnya',
			'full_tag_close' => '</ul></nav></div>',
			'full_tag_open' => '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">',
			'num_tag_open' => '<li class="page-item"><span class="page-link">',
			'num_tag_close' => '</span></li>',
			'cur_tag_open' => '<li class="page-item active"><span class="page-link">',
			'cur_tag_close' => '<span class="sr-only">(current)</span></span></li>',
			'next_tag_open' => '<li class="page-item"><span class="page-link">',
			'next_tagl_close' => '<span aria-hidden="true">&raquo;</span></span></li>',
			'prev_tag_open' => '<li class="page-item"><span class="page-link">',
			'prev_tagl_close' => '</span>Next</li>',
			'first_tag_open' => '<li class="page-item"><span class="page-link">',
			'first_tagl_close' => '</span></li>',
			'last_tag_open' => '<li class="page-item"><span class="page-link">',
			'last_tagl_close' => '</span></li>'
		];
		$this->pagination->initialize($config_pagination);
		$data = $this->db->order_by('id', 'desc')->get('blog', $config_pagination['per_page'], $this->uri->segment(3))->result();
		view('blog.beranda', [
			'data' => $data,
			'ci' => $this->ci
		]);
	}

	public function tulis_baru(){
		$data = $this->db->order_by('nama_kategori')->get('kategori')->result();
		view('blog.tulis-baru', [
			'data' => $data,
			'ci' => $this->ci
		]);
	}

	public function posting(){
		$this->db->insert('blog', [
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
			'kategori_id' => $this->input->post('kategori_id'),
			'created_at' => date('d-m-Y H:i:s'),
			'updated_at' => date('d-m-Y H:i:s')
		]);
		redirect('/');
	}

	public function hapus($id){
		$this->db->delete('blog', [
			'id' => $id
		]);
		redirect('/');
	}

	public function full($id){
		$data = $this->db->where([
			'id' => $id
		])->get('blog')->result();
		view('blog.full', [
			'data' => $data,
			'ci' => $this->ci
		]);
	}

	public function edit($id){
		$data = $this->db->where([
			'id' => $id
		])->get('blog')->result();
		$kategori = $this->db->order_by('nama_kategori')->get('kategori')->result();
		view('blog.edit', [
			'data' => $data,
			'kategori' => $kategori,
			'ci' => $this->ci
		]);
	}

	public function selesai_edit($id){
		$this->db->update('blog', [
			'judul' => $this->input->post('judul'),
			'isi' => $this->input->post('isi'),
			'kategori_id' => $this->input->post('kategori_id'),
			'updated_at' => date('d-m-Y H:i:s')
		], [
			'id' => $id
		]);
		redirect('/blog/full/' . $id);
	}

	public function tambah_kategori(){
		$this->db->insert('kategori', [
			'nama_kategori' => $this->input->post('kategori')
		]);
		redirect('/');
	}

	public function kategori($id){
		$segment_4 = $this->uri->segment(4);
		if ($segment_4 == null){
			redirect('/blog/kategori/' . $id . '/0');
		}
		$jumlah = count($this->db->where([
			'kategori_id' => $id
		])->get('blog')->result());
		$config_pagination = [
			'base_url' => '/blog/kategori/' . $id,
			'total_rows' => $jumlah,
			'per_page' => 10,
			'first_link' => 'Pertama',
			'last_link' => 'Terakhir',
			'next_link' => 'Berikutnya',
			'prev_link' => 'Sebelumnya',
			'full_tag_close' => '</ul></nav></div>',
			'full_tag_open' => '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">',
			'num_tag_open' => '<li class="page-item"><span class="page-link">',
			'num_tag_close' => '</span></li>',
			'cur_tag_open' => '<li class="page-item active"><span class="page-link">',
			'cur_tag_close' => '<span class="sr-only">(current)</span></span></li>',
			'next_tag_open' => '<li class="page-item"><span class="page-link">',
			'next_tagl_close' => '<span aria-hidden="true">&raquo;</span></span></li>',
			'prev_tag_open' => '<li class="page-item"><span class="page-link">',
			'prev_tagl_close' => '</span>Next</li>',
			'first_tag_open' => '<li class="page-item"><span class="page-link">',
			'first_tagl_close' => '</span></li>',
			'last_tag_open' => '<li class="page-item"><span class="page-link">',
			'last_tagl_close' => '</span></li>'
		];
		$this->pagination->initialize($config_pagination);
		$data = $this->db->where([
			'kategori_id' => $id
		])->order_by('id', 'desc')->select('blog.*, kategori.nama_kategori')->join('kategori', 'kategori.id = blog.kategori_id')->get('blog', $config_pagination['per_page'], $this->uri->segment(4))->result();
		view('blog.kategori', [
			'data' => $data,
			'ci' => $this->ci,
			'kategori_id' => $id
		]);
	}

	public function hapus_kategori($id){
		$this->db->delete('kategori', [
			'id' => $id
		]);
		redirect('/');
	}

	public function cari(){
		$query = $this->input->get('cari');
		$data = $this->db->like('judul', $query)->or_like('isi', $query)->get('blog')->result();
		view('blog.cari', [
			'data' => $data,
			'ci' => $this->ci,
			'query' => $query
		]);
	}

	public function presentasi($id){
		$data = $this->db->where([
			'id' => $id
		])->get('blog')->result();
		view('blog.presentasi', [
			'data' => $data,
			'ci' => $this->ci
		]);
	}
}
