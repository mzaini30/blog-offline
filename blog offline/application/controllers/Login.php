<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->ci =& get_instance();
	}
	public function index()
	{
		$this->beranda();
	}

	public function beranda(){
		view('login.login', [
			'ci' => $this->ci
		]);
	}

	public function cek(){
		$data = $this->db->get('password')->result();
		$input_password = $this->input->post('password');
		if ($input_password == $data[0]->password){
			$this->session->set_userdata([
				'login' => 'login'
			]);
			redirect('/');
		} else {
			redirect('/login');
		}
	}

	public function keluar(){
		$this->session->sess_destroy();
		redirect('/');
	}
}
