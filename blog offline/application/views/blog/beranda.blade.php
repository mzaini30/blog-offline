@extends('layout.blog')

@section('judul')
	Beranda
@endsection

@section('konten')
	@foreach ($data as $d)
		<div class="panel panel-default">
			<div class="panel-heading list-group-item">{{ $d->judul }} <a href="/blog/kategori/{{ $d->kategori_id }}" class="badge badge-kategori">{{ $d->kategori_id }}</a></div>
			<div class="panel-body">
				@if (strpos($d->judul, '(presentasi)') !== false)
					<p><a href="/blog/presentasi/{{ $d->id }}" class="btn btn-success" target="_blank">Tampilkan Presentasi</a></p>
				@endif
				<p>{!! $ci->markdown->parse($d->isi) !!}</p>
				<br><p><em>{{ strftime('%A, %d %B %Y | %H.%M.%S', strtotime($d->created_at)) }}</em></p>
				<p><em>@if ($d->created_at != $d->updated_at)
					{{ strftime('%A, %d %B %Y | %H.%M.%S', strtotime($d->updated_at)) }}
				@endif</em></p>
				<hr>
				<a href="/blog/full/{{ $d->id }}" class="btn btn-default btn-sm">lihat full</a>
				<a href="/blog/edit/{{ $d->id }}" class="btn btn-warning btn-sm">edit</a>
				<a href="/blog/hapus/{{ $d->id }}" class="btn btn-danger btn-sm">hapus</a>
			</div>
		</div>
	@endforeach
	<center>
		{!! $ci->pagination->create_links() !!}
	</center>
@endsection

@section('footer')
	@include('include.theia')
	@include('include.highlight')
@endsection