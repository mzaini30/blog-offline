@extends('layout.tambah-header')

@section('judul')
	Edit {{ $data[0]->judul }}
@endsection

@section('setelah-header')
	<div class="container">
		<form method="post" action="/blog/selesai_edit/{{ $data[0]->id }}">
			<div class="row">
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="form-group"><input type="text" placeholder="Masukkan judul" name="judul" class="form-control" autofocus="" value="{{ $data[0]->judul }}"></div>
						<div class="form-group"><textarea name="isi" id=""  rows="10" class="form-control markdown-input">{{ $data[0]->isi }}</textarea></div>
						<div class="form-group"><select name="kategori_id" id="" class="form-control">
							@foreach ($kategori as $k)
								<option value="{{ $k->id }}" @if ($data[0]->kategori_id == $k->id)
									selected="" 
								@endif>{{ $k->nama_kategori }}</option>
							@endforeach
						</select></div>
						<input type="submit" value="Update" class="btn btn-success">
					</div>
				</div>
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="markdown-output"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('footer')
	@include('include.theia')
	@include('include.markdown')
@endsection