@extends('layout.blog')

@section('judul')
	{{ $data[0]->judul }}
@endsection

@section('konten')
	<div class="panel panel-default">
		<div class="panel-heading list-group-item">{{ $data[0]->judul }} <a href="/blog/kategori/{{ $data[0]->kategori_id }}" class="badge badge-kategori">{{ $data[0]->kategori_id }}</a></div>
		<div class="panel-body">
			@if (strpos($data[0]->judul, '(presentasi)') !== false)
				<p><a href="/blog/presentasi/{{ $data[0]->id }}" class="btn btn-success" target="_blank">Tampilkan Presentasi</a></p>
			@endif
			<p>{!! $ci->markdown->parse($data[0]->isi) !!}</p>
			<br><p><em>{{ strftime('%A, %d %B %Y | %H.%M.%S', strtotime($data[0]->created_at)) }}</em></p>
				<p><em>@if ($data[0]->created_at != $data[0]->updated_at)
					{{ strftime('%A, %d %B %Y | %H.%M.%S', strtotime($data[0]->updated_at)) }}
				@endif</em></p>
			<hr>
			<a href="/blog/edit/{{ $data[0]->id }}" class="btn btn-warning btn-sm">edit</a>
			<a href="/blog/hapus/{{ $data[0]->id }}" class="btn btn-danger btn-sm">hapus</a>
		</div>
	</div>
@endsection

@section('footer')
	@include('include.theia')
	@include('include.highlight')
@endsection