@extends('layout.tambah-header')

@section('judul')
	Tulis Baru
@endsection

@section('setelah-header')
	<div class="container">
		<form method="post" action="/blog/posting">
			<div class="row">
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="form-group"><input type="text" placeholder="Masukkan judul" name="judul" class="form-control" autofocus=""></div>
						<div class="form-group"><textarea name="isi" id=""  rows="10" class="form-control markdown-input" placeholder="Tulis di sini. Gunakan Markdown"></textarea></div>
						<div class="form-group"><select name="kategori_id" id="" class="form-control">
							@foreach ($data as $d)
								<option value="{{ $d->id }}">{{ $d->nama_kategori }}</option>
							@endforeach
						</select></div>
						<input type="submit" value="Posting" class="btn btn-success">
					</div>
				</div>
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="markdown-output"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('footer')
	@include('include.theia')
	@include('include.markdown')
@endsection