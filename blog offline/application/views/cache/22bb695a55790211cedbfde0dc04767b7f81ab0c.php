<?php $__env->startSection('setelah-header'); ?>
	<div class="container">
		<form method="post" action="/blog/posting">
			<div class="form-group"><input type="text" placeholder="Masukkan judul" name="judul" class="form-control" autofocus=""></div>
			<div class="row">
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="form-group"><textarea name="isi" id=""  rows="10" class="form-control markdown-input"></textarea></div>
						<div class="form-group"><select name="kategori_id" id="" class="form-control">
							<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($d->id); ?>"><?php echo e($d->nama_kategori); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select></div>
						<input type="submit" value="Posting" class="btn btn-success">
					</div>
				</div>
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="markdown-output"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.tambah-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>