<form action="">
	<div class="form-group"><input type="text" placeholder="Apa yang mau kamu cari?" class="form-control"></div>
</form>

<div class="panel panel-default">
	<div class="panel-heading">Admin</div>
	<div class="list-group">
		<a href="/blog/tulis_baru" class="list-group-item">Tambah Tulisan</a>
		<a href="#!" class="list-group-item">Ganti Password</a>
		<a href="/login/keluar" class="list-group-item">Keluar</a>
	</div>
</div>

<?php $kategori = $ci->db->order_by('nama_kategori')->get('kategori')->result() ?>

<div class="panel panel-default">
	<div class="panel-heading">Kategori</div>
	<div class="list-group">
		<?php $__currentLoopData = $kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<a href="/blog/kategori/<?php echo e($k->id); ?>" class="list-group-item"><?php echo e($k->nama_kategori); ?></a>
		<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</div>
</div>