<?php $__env->startSection('judul'); ?>
	<?php echo e($data[0]->judul); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>
	<div class="panel panel-default">
		<div class="panel-heading list-group-item"><?php echo e($data[0]->judul); ?> <a href="/blog/kategori/<?php echo e($data[0]->kategori_id); ?>" class="badge badge-kategori"><?php echo e($data[0]->kategori_id); ?></a></div>
		<div class="panel-body">
			<?php if(strpos($data[0]->judul, '(presentasi)') !== false): ?>
				<p><a href="/blog/presentasi/<?php echo e($data[0]->id); ?>" class="btn btn-success" target="_blank">Tampilkan Presentasi</a></p>
			<?php endif; ?>
			<p><?php echo $ci->markdown->parse($data[0]->isi); ?></p>
			<br><p><em><?php echo e(strftime('%A, %d %B %Y | %H.%M.%S', strtotime($data[0]->created_at))); ?></em></p>
				<p><em><?php if($data[0]->created_at != $data[0]->updated_at): ?>
					<?php echo e(strftime('%A, %d %B %Y | %H.%M.%S', strtotime($data[0]->updated_at))); ?>

				<?php endif; ?></em></p>
			<hr>
			<a href="/blog/edit/<?php echo e($data[0]->id); ?>" class="btn btn-warning btn-sm">edit</a>
			<a href="/blog/hapus/<?php echo e($data[0]->id); ?>" class="btn btn-danger btn-sm">hapus</a>
		</div>
	</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
	<?php echo $__env->make('include.theia', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('include.highlight', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>