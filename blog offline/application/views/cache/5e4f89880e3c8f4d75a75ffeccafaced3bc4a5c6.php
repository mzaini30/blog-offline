<?php $__env->startSection('setelah-header'); ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-8 theia">
				<div class="theiaStickySidebar">
					<?php echo $__env->yieldContent('konten'); ?>
				</div>
			</div>
			<div class="col-sm-4 theia">
				<div class="theiaStickySidebar">
					<?php echo $__env->make('include.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.tambah-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>