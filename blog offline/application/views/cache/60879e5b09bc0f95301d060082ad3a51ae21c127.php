<!DOCTYPE html>
<html>
  <head>
    <title><?php echo e($data[0]->judul); ?> - Zen</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
  </head>
  <body>
    <textarea id="source" style="display: none;">

<?php echo e($data[0]->isi); ?>


    </textarea>
    <script src="/bin/remark-latest.min.js">
    </script>
    <script>
      var slideshow = remark.create();
    </script>
  </body>
</html>