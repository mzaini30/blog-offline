<!DOCTYPE html>
<html>
<head>
	<title><?php echo $__env->yieldContent('judul', ''); ?> - Zen</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/bin/style.css">
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
</head>
<body>
	<?php echo $__env->yieldContent('isi'); ?>
	<br>

	<script type="text/javascript" src="/bin/jquery.min.js"></script>
	<?php $kategori = $ci->db->get('kategori')->result() ?>
	<script type="text/javascript">
		$('.badge-kategori').each(function(){
			<?php $__currentLoopData = $kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				if ($(this).html() == '<?php echo e($k->id); ?>'){
					$(this).html('<?php echo e($k->nama_kategori); ?>')
				}
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		})
	</script>
	<?php echo $__env->yieldContent('footer'); ?>
</body>
</html>