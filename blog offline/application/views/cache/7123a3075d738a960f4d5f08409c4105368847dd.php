<?php $__env->startSection('setelah-header'); ?>
	<div class="container">
		<form method="post" action="/blog/selesai_edit/<?php echo e($data[0]->id); ?>">
			<div class="form-group"><input type="text" placeholder="Masukkan judul" name="judul" class="form-control" autofocus="" value="<?php echo e($data[0]->judul); ?>"></div>
			<div class="row">
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="form-group"><textarea name="isi" id=""  rows="10" class="form-control markdown-input"><?php echo e($data[0]->isi); ?></textarea></div>
						<div class="form-group"><select name="kategori_id" id="" class="form-control">
							<?php $__currentLoopData = $kategori; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<option value="<?php echo e($k->id); ?>" <?php if($data[0]->kategori_id == $k->id): ?>
									selected="" 
								<?php endif; ?>><?php echo e($k->nama_kategori); ?></option>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</select></div>
						<input type="submit" value="Posting" class="btn btn-success">
					</div>
				</div>
				<div class="col-sm-6 theia">
					<div class="theiaStickySidebar">
						<div class="markdown-output"></div>
					</div>
				</div>
			</div>
		</form>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.tambah-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>