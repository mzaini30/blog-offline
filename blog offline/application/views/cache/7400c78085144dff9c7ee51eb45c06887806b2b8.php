<!DOCTYPE html>
<html>
<head>
	<title>Zen</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<?php echo $__env->yieldContent('isi'); ?>
	<br>

	<script type="text/javascript" src="/bin/jquery.min.js"></script>
	<script type="text/javascript" src="/bin/ResizeSensor.min.js"></script>
	<script type="text/javascript" src="/bin/theia-sticky-sidebar.min.js"></script>
	<script type="text/javascript" src="/bin/marked.js"></script>
	<script type="text/javascript">
		$('.theia').theiaStickySidebar({
			additionalMarginTop: 20,
			additionalMarginBottom: 20
		})

		markdown_jalankan = function(){
			$('.markdown-output').html(marked($('.markdown-input').val()))
		}
		markdown_jalankan()
		$(document).on('keyup', '.markdown-input', function(){
			markdown_jalankan()
		})
	</script>
</body>
</html>