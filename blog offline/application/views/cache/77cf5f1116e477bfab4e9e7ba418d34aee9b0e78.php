<?php $__env->startSection('konten'); ?>
	<div class="panel panel-default">
		<div class="panel-heading list-group-item"><?php echo e($data[0]->judul); ?> <div class="badge"><?php echo e($data[0]->kategori_id); ?></div></div>
		<div class="panel-body">
			<p><?php echo $ci->markdown->parse($data[0]->isi); ?></p>
			<hr>
			<a href="/blog/edit/<?php echo e($data[0]->id); ?>" class="btn btn-warning btn-sm">edit</a>
			<a href="/blog/hapus/<?php echo e($data[0]->id); ?>" class="btn btn-danger btn-sm">hapus</a>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>