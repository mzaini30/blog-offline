<?php $__env->startSection('konten'); ?>
	<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="panel panel-default">
			<div class="panel-heading list-group-item"><?php echo e($d->judul); ?> <div class="badge"><?php echo e($d->kategori_id); ?></div></div>
			<div class="panel-body">
				<p><?php echo $ci->markdown->parse($d->isi); ?></p>
				<hr>
				<a href="/blog/full/<?php echo e($d->id); ?>" class="btn btn-default btn-sm">lihat full</a>
				<a href="/blog/edit/<?php echo e($d->id); ?>" class="btn btn-warning btn-sm">edit</a>
				<a href="/blog/hapus/<?php echo e($d->id); ?>" class="btn btn-danger btn-sm">hapus</a>
			</div>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>