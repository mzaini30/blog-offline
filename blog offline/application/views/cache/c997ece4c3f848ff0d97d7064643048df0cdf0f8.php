<?php $__env->startSection('judul'); ?>
	Beranda
<?php $__env->stopSection(); ?>

<?php $__env->startSection('konten'); ?>
	<?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $d): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
		<div class="panel panel-default">
			<div class="panel-heading list-group-item"><?php echo e($d->judul); ?> <a href="/blog/kategori/<?php echo e($d->kategori_id); ?>" class="badge badge-kategori"><?php echo e($d->kategori_id); ?></a></div>
			<div class="panel-body">
				<?php if(strpos($d->judul, '(presentasi)') !== false): ?>
					<p><a href="/blog/presentasi/<?php echo e($d->id); ?>" class="btn btn-success" target="_blank">Tampilkan Presentasi</a></p>
				<?php endif; ?>
				<p><?php echo $ci->markdown->parse($d->isi); ?></p>
				<br><p><em><?php echo e(strftime('%A, %d %B %Y | %H.%M.%S', strtotime($d->created_at))); ?></em></p>
				<p><em><?php if($d->created_at != $d->updated_at): ?>
					<?php echo e(strftime('%A, %d %B %Y | %H.%M.%S', strtotime($d->updated_at))); ?>

				<?php endif; ?></em></p>
				<hr>
				<a href="/blog/full/<?php echo e($d->id); ?>" class="btn btn-default btn-sm">lihat full</a>
				<a href="/blog/edit/<?php echo e($d->id); ?>" class="btn btn-warning btn-sm">edit</a>
				<a href="/blog/hapus/<?php echo e($d->id); ?>" class="btn btn-danger btn-sm">hapus</a>
			</div>
		</div>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	<center>
		<?php echo $ci->pagination->create_links(); ?>

	</center>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('footer'); ?>
	<?php echo $__env->make('include.theia', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	<?php echo $__env->make('include.highlight', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout.blog', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>