<form action="/blog/cari">
	<div class="form-group"><input type="text" name="cari" placeholder="Apa yang mau kamu cari?" class="form-control"></div>
</form>

<div class="panel panel-default">
	<div class="panel-heading">Tools</div>
	<div class="list-group">
		<a href="/tools/Online FlowChart & Diagrams Editor - Mermaid Live Editor.html" target="_blank" class="list-group-item">Mermaid</a>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Admin</div>
	<div class="list-group">
		<a href="/blog/tulis_baru" class="list-group-item">Tambah Tulisan</a>
		<a href="#!" class="list-group-item">Ganti Password</a>
		<a href="/login/keluar" class="list-group-item">Keluar</a>
	</div>
</div>

<form action="/blog/tambah_kategori" method="post">
	<div class="form-group"><input type="text" name="kategori" placeholder="Tambahkan kategori. Gunakan huruf kecil" class="form-control"></div>
</form>

<?php 
$kategori = $ci->db->order_by('nama_kategori')->get('kategori')->result();
?>

<div class="panel panel-default">
	<div class="panel-heading">Kategori</div>
	<div class="list-group">
		@foreach ($kategori as $k)
			<a href="/blog/kategori/{{ $k->id }}" class="list-group-item">{{ ucwords($k->nama_kategori) }}</a>
		@endforeach
	</div>
</div>