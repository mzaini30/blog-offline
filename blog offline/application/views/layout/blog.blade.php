@extends('layout.tambah-header')

@section('setelah-header')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 theia">
				<div class="theiaStickySidebar">
					@yield('konten')
				</div>
			</div>
			<div class="col-sm-4 theia">
				<div class="theiaStickySidebar">
					@include('include.sidebar')
				</div>
			</div>
		</div>
	</div>
@endsection