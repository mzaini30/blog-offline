<!DOCTYPE html>
<html>
<head>
	<title>@yield('judul', '') - Zen</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/bin/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/bin/style.css">
	<link rel="icon" type="image/x-icon" href="/favicon.ico">
</head>
<body>
	@yield('isi')
	<br>

	<script type="text/javascript" src="/bin/jquery.min.js"></script>
	<?php $kategori = $ci->db->get('kategori')->result() ?>
	<script type="text/javascript">
		$('.badge-kategori').each(function(){
			@foreach ($kategori as $k)
				if ($(this).html() == '{{ $k->id }}'){
					$(this).html('{{ $k->nama_kategori }}')
				}
			@endforeach
		})
	</script>
	@yield('footer')
</body>
</html>