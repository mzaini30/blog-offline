@extends('layout.kosongan')

@section('isi')
	<div class="navbar navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a href="/" class="navbar-brand">Zen</a>
			</div>
		</div>
	</div>
	@yield('setelah-header')
@endsection