@extends('layout.tambah-header')

@section('judul')
	Login
@endsection

@section('setelah-header')
	<div class="container">
		<form action="/login/cek" method="post">
			<div class="form-group"><input autofocus="" type="password" class="form-control" name="password" placeholder="Masukkan password"></div>
		</form>
	</div>
@endsection